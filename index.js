/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function yourInfo(){
		let fullName = prompt("Enter your full name")
		let age = prompt("Enter your age")
		let location = prompt("Enter your location")
		console.log("Full Name: "+ fullName);
		console.log("Age: "+ age);
		console.log("Location:" + location);

	}
	yourInfo();

	function fillupDone(){
		alert("Thank you for your input!")
	}

	fillupDone();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function top5faveBands(){
		console.log("My Top 5 Favorite Bands");
		console.log("Top 5: Pedicab");
		console.log("Top 4: <S> Sandwich");
		console.log("Top 3: Paramore");
		console.log("Top 2: Eraserheads");
		console.log("Top 1: My Chemical Romance");
	};

	top5faveBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function top5faveMovies(){
		console.log("My Top 5 Favorite Movies");
		console.log("Top 5: A Million Ways to Die in the West - IMBD Rating: 6.1/10");
		console.log("Top 4: Star Wars Ep. 3 - IMBD Rating: 7.6/10");
		console.log("Top 3: Borat - IMBD Rating: 7.3/10");
		console.log("Top 2: The Brothers Grimsby - IMBD Rating: 6.2/10");
		console.log("Top 1: Avengers: End Game - IMBD Rating: 8.4/10");
	};

	top5faveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


// let printFriends();
	function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printUsers();



// console.log(friend1);
// console.log(friend2);